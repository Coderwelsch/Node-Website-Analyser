// imports
var Path = require('path'),
    ChildProcess = require('child_process'),
    PhantomJS = require('phantomjs'),
    BinPath = PhantomJS.path,
    ChildArgs = [
        Path.join( __dirname, 'phantomjs-script.js' ),
        'some other argument (passed to phantomjs script)'
    ];

ChildProcess.execFile( BinPath, ChildArgs, function( error, stdout, stderr ) {
    console.log( error );
} );
